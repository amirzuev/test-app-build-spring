FROM openshift/base-centos7:latest 
RUN yum -y install epel-release; yum clean all
ENV JAVA_VERSION 1.8.0
ENV JAVA_HOME /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.191.b12-1.el7_6.x86_64/jre
RUN yum -y install java-${JAVA_VERSION}-openjdk.x86_64 java-${JAVA_VERSION}-openjdk-devel.x86_64 java-${JAVA_VERSION}-openjdk-headless && yum clean all
RUN yum -y install wget curl git && yum clean all
RUN mkdir -p /opt/app
ENV ARTFILE main.jar
COPY ${ARTFILE} /opt/app
EXPOSE 8080
   
CMD ["java","-jar","/opt/app/${ARTFILE}"]
