package appdev.org.testappbuildspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//  server config 
import org.springframework.cloud.config.server.EnableConfigServer;
// import org.springframework.boot.context.properties.EnableConfigurationProperties;

// @EnableConfigServer
@SpringBootApplication
public class TestAppBuildSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestAppBuildSpringApplication.class, args);
	}
}
