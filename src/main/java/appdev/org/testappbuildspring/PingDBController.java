package appdev.org.testappbuildspring;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
// import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.beans.factory.annotation.Value;

// import org.springframework.stereotype.Service;

// @RefreshScope
@Configuration
@RestController
public class PingDBController {

	@Value("${message}")
	private String message;
	
	
	@Value("${message_beta:VVV}")
	private String message_beta;

    
	@RequestMapping("/ping_db")
    public String index() {
        return "pong";
    }

	@RequestMapping("/test_profile")
	public String test() {
		return this.message;
	}
	@Profile("dev")
	@RequestMapping("v1beta/test_profile")
	public String test_dev(){
		return this.message_beta;
	}

}
