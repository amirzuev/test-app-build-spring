package appdev.org.testappbuildspring;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Profile("dev")
@RestController
public class PingController {

    @RequestMapping("/ping")
    public String index() {
        return "pong";
    }

}
